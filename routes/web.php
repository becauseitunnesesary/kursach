<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index']
)->name('index');

Route::get('/tovar/{id}', [MainController::class, 'sort']
)->name('sort');

Route::get('/find', function () {
    return view('find');
})->name('find');

Route::get('/find/result', [MainController::class, 'show']
)->name('find-form');

Route::middleware(['auth'])->group(function () {
    Route::resource('admin/tovar', \App\Http\Controllers\AdminController::class, ['parameters' => [
        'tovar' => 'id']]);
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes([
    'register' => false,
    'reset' => false,
    'confirm' => false,
    'verify' => false,
]);
