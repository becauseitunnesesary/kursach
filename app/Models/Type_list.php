<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Type_list extends Model
{
    use HasFactory;
    protected $table = "type_list";
    public $timestamps = false;

    public function Types(){
        return DB::table('type_list')->get();
    }
}
