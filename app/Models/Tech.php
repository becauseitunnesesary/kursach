<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tech extends Model
{
    use HasFactory;

    protected $table = "tech";
    public $timestamps = false;

    public function query_build(){
        return DB::table('tech', 't')
            ->join('type_list as tl', 'tl.TypeID', '=', 't.TypeID')
            ->select('t.*', 'tl.*');
    }

    public function AllTovar()
    {
        return $this->query_build()
            ->get();
    }

    public function OneTovar($name)
    {
        return $this->query_build()
            ->where('t.Name', 'like', '%' . $name . '%')
            ->get();
    }

    public function OneTovarById($id)
    {
        return $this->query_build()
            ->Where('t.ID', '=', $id)
            ->get();
    }

    public function Category($id)
    {
        return $this->query_build()
            ->Where('t.TypeID', $id)
            ->get();
    }
}
