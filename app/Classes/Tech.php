<?php

namespace App\Classes;

class Tech
{
    public $ID;
    public $Name;
    public $Cost;
    public $TypeID;
    public $YearID;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID): void
    {
        $this->ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->Cost;
    }

    /**
     * @param mixed $Cost
     */
    public function setCost($Cost): void
    {
        $this->Cost = $Cost;
    }

    /**
     * @return mixed
     */
    public function getTypeID()
    {
        return $this->TypeID;
    }

    /**
     * @param mixed $TypeID
     */
    public function setTypeID($TypeID): void
    {
        $this->TypeID = $TypeID;
    }

    /**
     * @return mixed
     */
    public function getYearID()
    {
        return $this->YearID;
    }

    /**
     * @param mixed $YearID
     */
    public function setYearID($YearID): void
    {
        $this->YearID = $YearID;
    }
}
