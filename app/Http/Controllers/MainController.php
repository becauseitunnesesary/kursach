<?php

namespace App\Http\Controllers;

use App\Models\Type_list;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use \App\Models\Tech;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        return view('home', ['result' => (new Tech)->AllTovar(),'types' => (new Type_list)->Types()]);
    }

    public function show(Request $req)
    {
        return view('find', ['result' => (new Tech)->OneTovar($req->input('name'))]);
    }

    public function sort($id)
    {
        return view('home', ['result' => (new Tech())->Category($id),'types' => (new Type_list)->Types()]);
    }
}
