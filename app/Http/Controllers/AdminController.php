<?php

namespace App\Http\Controllers;

use App\Models\Tech;
use App\Models\Type_list;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.index', ['result' => (new Tech)->AllTovar()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.add', ['types' => (new Type_list)->Types()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new Tech;
        $add->Name = $request->input('name');
        $add->Cost = $request->input('cost');
        $add->TypeID = $request->input('type');
        $add->YearID = $request->input('year');
        $add->save();
        return redirect()->route('tovar.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('Admin.info', ['result' => (new Tech)->OneTovarById($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Admin.edit', ['types' => (new Type_list)->Types(),
            'data' => (new Tech)->OneTovarById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('tech')
            ->where('ID', $id)
            ->update([
                'Name' => $request->input('name'),
                'Cost' => $request->input('cost'),
                'TypeID' => $request->input('type'),
                'YearID' => $request->input('year')]);
        return redirect()->route('tovar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        DB::table('tech')->where('ID', '=', $id)->delete();
        return redirect()->route('tovar.index');
    }
}
