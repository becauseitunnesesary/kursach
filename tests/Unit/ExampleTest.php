<?php

namespace Tests\Unit;

use \App\Classes\Tech;

use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    protected function setUp(): void
    {
        $this->tech = new Tech();
    }

    /**
     * @dataProvider typeDataProvider
     * @param $actual
     * @param $expected
     */

    public function testType($actual, $expected)
    {
        $result = $this->TypeID = $actual;
        $this->assertEquals($expected, $result);
    }

    public function typeDataProvider(): array
    {
        return array(
            array('datchik', 'datchik'),
            array('controller', 'controller'),
            array('monitor', 'monitor')
        );
    }

    protected function tearDown(): void
    {
    }
    public function testMock()
    {
        $stub = $this->getMockBuilder(Tech::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $stub->method('getName')
            ->willReturn('Samsung');

        $this->assertSame('Samsung', $stub->getName());
    }
}
