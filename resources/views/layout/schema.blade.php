<!doctype html>
<html lang="ua">
<head>
    <title>@yield('Title')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="center">
    <center><h1>Magazin electronicu</h1>
        @yield('Content')</center>

    @if (isset($result))
        <table>
            <thead>
            <tr>
                <th>Tovar Name</th>
                <th>Price</th>
                <th>Type</th>
                <th>Year</th>

            </tr>
            </thead>
            <tbody>

            @foreach ($result as $row)
                <tr>
                    <td> {{$row->Name}} </td>
                    <td> {{$row->Cost}} </td>
                    <td> {{$row->NameOfType}} </td>
                    <td> {{$row->YearID}} </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
</body>
</html>

