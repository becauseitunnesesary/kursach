@extends('layout.schema')

@section('Title','Find')

@section('Content')
    <p>
    <form action="{{route('find-form')}}">
        @csrf
        <input name="name" id="name" type="text">
        <button id="find" type="submit">Find</button>

        <a href="{{route('index')}}">Back</a>
    </form>
    </p>
@endsection
