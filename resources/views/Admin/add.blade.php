@extends('Admin.layout.schema')

@section('Title','Add')
@section('Content')
    <form action="{{route('tovar.store')}}" method="post">
        @csrf
        <p>
            <input placeholder="Name" type="text" name="name">
        </p>
        <p>
            <input placeholder="Cost" type="text" name="cost">
        </p>
        <p>
            <select name="type" required>
                @foreach($types as $type)
                    <option value='{{ $type->TypeID }}'>{{ $type->NameOfType}}</option>
                @endforeach
            </select>
        </p>
        <p>
            <input placeholder="Year" type="text" name="year">
        </p>
        <p>
        <button type="submit">Add</button>
        <a href="{{route('tovar.index')}}">Back </a>
        </p>
    </form>
@endsection


