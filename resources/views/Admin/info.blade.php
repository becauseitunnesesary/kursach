@extends('Admin.layout.schema')
@foreach($result as $row)
    @section('Title',$row->Name)
@section('thead')
    <th>Price</th>
    <th>Type</th>
    <th>Year</th>
@endsection
@section('tbody')
    <td> {{$row->Cost}} </td>
    <td> {{$row->NameOfType}} </td>
    <td> {{$row->YearID}} </td>
@endsection
@endforeach
