<!doctype html>
<html lang="ua">
<head>
    <title>@yield('Title')</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="center">
    <center><h1>Admin Panel</h1>

        <a href="{{route('tovar.index')}}">Main</a>
        <a href="{{route('tovar.create')}}">Add</a>
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>

    </center>
    @yield('Content')
    @if  (isset($result))
        <table>
            <thead>
            <tr>
                <th>Tovar Name</th>
                @yield('thead')
                <th>Action</th>

            </tr>
            </thead>
            <tbody>

            @foreach ($result as $row)
                <tr>
                    <td> {{$row->Name}} </td>
                    @yield('tbody')
                    <td>
                        <a href="{{route('tovar.show',['id' => $row->ID])}}">More</a>
                        <a href="{{route('tovar.edit',['id' => $row->ID]) }}">Edit</a>
                        <form action="{{ route('tovar.destroy' ,['id' => $row->ID])}}" method="POST">
                            <input name="_method" type="hidden" value="DELETE">
                            {{ csrf_field() }}
                            <button type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
</div>
</body>
</html>

