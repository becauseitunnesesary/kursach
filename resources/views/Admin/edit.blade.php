@extends('Admin.layout.schema')
@foreach($data as $row)
@section('Title','Edit '.$row->Name)
@section('Content')
    <form action="{{route('tovar.update',['id'=>$row->ID])}}" method="post">
        <input name="_method" type="hidden" value="PUT">
        @csrf
        <p>
            <input placeholder="Name" type="text" name="name" value="{{$row->Name}}">
        </p>
        <p>
            <input placeholder="Cost" type="text" name="cost" value="{{$row->Cost}}">
        </p>
        <p>
            <select name="type" required>
                @foreach($types as $type)
                    <option value='{{ $type->TypeID }}'>{{ $type->NameOfType}}</option>
                @endforeach
            </select>
        </p>
        <p>
            <input placeholder="Year" type="text" name="year" value="{{$row->YearID}}">
        </p>
        <p>
        <button type="submit">Edit</button>
        <a href="{{route('tovar.index')}}">Back </a>
        </p>
    </form>
@endsection
@endforeach


