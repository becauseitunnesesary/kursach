@extends('layout.schema')

@section('Title','Main')

@section('Content')
    <p>
        <a href="{{route('find')}}">Find</a>|
        <a href="{{ route('login') }}">{{ __('Login') }}</a>
    </p>
    @if(isset($types))
        @foreach($types as $type)
            <a href="{{route('sort',['id'=>$type->TypeID])}}">{{$type->NameOfType}}
                <a>
        @endforeach
    @endif
@endsection
